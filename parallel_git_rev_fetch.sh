#!/bin/bash
#
# $1 -- file with "dir_name git_url revision" triplets (space-separated) on individual lines
#       (optional; reads from stdin by default)
#

function fetch_git_rev() {
  # $1 -- directory name
  # $2 -- git repository URL
  # $3 -- revision to fetch

  local dir_name="$1"
  local git_url="$2"
  local rev="$3"

  mkdir -p "$dir_name" &&
  cd "$dir_name" &&
  git init >/dev/null 2>&1 &&

  git remote add origin "$git_url" &&
  {
    retries=5
    while [ $retries -gt 0 ]; do
      if ! git fetch origin "$3"; then
        echo "Retrying" >&2
        retries=$((retries - 1))
      else
        break
      fi
    done
    test $retries -gt 0
  } &&
  git checkout -b "$3" FETCH_HEAD &&
  git submodule update --init
}
export -f fetch_git_rev

function record_entry() {
  # $1 -- directory name
  # $2 -- git repository URL
  # $3 -- revision to fetch

  echo "$1" >> .$$.dirs
  echo "$2" >> .$$.urls
  echo "$3" >> .$$.revs
}

if [ $# -gt 0 ]; then
  exec 0<"$1"
fi

while read line; do
  if [ -n "$line" ]; then
    record_entry $line  # intentionally expanding line to 3 arguments
  fi
done

parallel -j0 --tagstring '{1}:' fetch_git_rev '{1}' '{2}' '{3}' :::: .$$.dirs ::::+ .$$.urls ::::+ .$$.revs
ret=$?

rm -f .$$.*

exit $ret
