import { ensureFile } from "https://deno.land/std/fs/ensure_file.ts";
import init, {
  parse,
  stringify,
} from "https://deno.land/x/yaml_wasm@0.1.9/index.js";
const { chdir, remove, run, readDirSync, readTextFile, writeTextFile } = Deno;
await init();

const folder = Deno.args[0];

const errorHandler = async (result: Number, file: any) => {
  if (result !== 0) {
    const rawError = await file.stderrOutput();
    const errorString = new TextDecoder().decode(rawError);
    console.log(`error: ${errorString}`);
    await file.close();
    return Promise.reject(errorString);
  }
  return Promise.resolve();
};

const standardCommandArgs = {
  stdout: "piped",
  stderr: "piped",
};

const createPr = async (repoFolder: string, repoName: string) => {
  chdir(repoFolder);
  let cmd = run({ ...standardCommandArgs, cmd: ["git", "add", ".github"] });
  const { code: addCode } = await cmd.status();
  await errorHandler(addCode, cmd);
  cmd.close();
  cmd = run(
    { ...standardCommandArgs, cmd: ["git", "rm", "-r", ".dependabot"] },
  );
  const { code: removeCode } = await cmd.status();
  await errorHandler(removeCode, cmd);
  cmd.close();
  cmd = run(
    {
      ...standardCommandArgs,
      cmd: [
        "git",
        "commit",
        "-sm",
        "migrated dependabot to stable config location\nChangelog: None\n",
      ],
    },
  );
  const { code: commitCode } = await cmd.status();
  await errorHandler(commitCode, cmd);
  cmd.close();

  const command = [
    "gh",
    "pr",
    "create",
    "--title",
    '"dependabot update"',
    "--body",
    '"migrated to stable dependabot release"',
    "--base",
    "master",
    "--draft",
    "--repo",
    `mendersoftware/${repoName}`,
  ];
  // const command = ['git', 'push', 'origin', 'fix/dependabot', '--force'];
  console.log(command.join(" "));
  cmd = run({ ...standardCommandArgs, cmd: command });
  const { code } = await cmd.status();
  await errorHandler(code, cmd);
  cmd.close();
};

const getRepoName = async (repoName: string): Promise<string> => {
  chdir(repoName);
  const repo = run({ ...standardCommandArgs, cmd: ["git", "remote", "-v"] });

  const { code } = await repo.status();
  await errorHandler(code, repo);
  const output = await repo.output();
  const outStr = new TextDecoder().decode(output);
  repo.close();
  // console.log(`outstr: ${outStr}`);
  const github = "mendersoftware/";
  const plainName = outStr.split(/\n/).reduce((accu, item) => {
    if (item.startsWith("mender	") && item.endsWith("(fetch)")) {
      // console.log(`item: ${item}`);
      const plainName = item.substring(
        item.indexOf(github) + github.length,
        item.indexOf(" (fetch)"),
      );
      return plainName.substring(
        0,
        plainName.indexOf(".git") > -1 ? plainName.indexOf(".git") : undefined,
      );
    }
    return accu;
  }, "");
  return Promise.resolve(plainName);
};

const packageManagerMapper: { [char: string]: string } = {
  javascript: "npm",
  python: "pip",
  docker: "docker",
  "go:modules": "gomod",
};

interface UpdateConfigs {
  package_manager: string;
  directory: string;
  update_schedule: string;
}

const adjustDependabotFiles = async (repo: string) => {
  chdir(repo);
  const target = `${repo}/.github/dependabot.yml`;
  const source = `${repo}/.dependabot`;
  await ensureFile(target);
  console.log(target);
  const configText = await readTextFile(`${source}/config.yml`);
  let [config] = parse(configText);
  config.version = 2;
  config.updates = config.update_configs.map((item: UpdateConfigs) => ({
    "commit-message": {
      prefix: "Changelog:All",
    },
    directory: item.directory,
    "package-ecosystem": packageManagerMapper[item.package_manager],
    schedule: {
      interval: item.update_schedule,
    },
  }));
  delete config.update_configs;
  const prettyConfig = stringify(config).split(/\n/).slice(1).concat("").join(
    "\n",
  );
  await writeTextFile(target, prettyConfig);
  await remove(`${source}/config.yml`);
  return await remove(source);
};

const createBranch = async (repo: string) => {
  chdir(repo);
  console.log("setting up new branch");
  const branch = run(
    {
      ...standardCommandArgs,
      cmd: ["git", "checkout", "-B", "fix/dependabot"],
    },
  );
  const { code: branchCode } = await branch.status();
  await errorHandler(branchCode, branch);
  return await branch.close();
};

const restoreRepo = async (
  repo: string,
  prevBranch: string,
  stashed: boolean,
) => {
  chdir(repo);
  console.log(`restoring: ${repo}`);
  let cmd = run(
    { ...standardCommandArgs, cmd: ["git", "checkout", prevBranch] },
  );
  const { code: branchCode } = await cmd.status();
  await errorHandler(branchCode, cmd);
  await cmd.close();
  if (stashed) {
    cmd = run({ ...standardCommandArgs, cmd: ["git", "stash", "pop"] });
    const { code: stashCode } = await cmd.status();
    await errorHandler(stashCode, cmd);
    await cmd.close();
  }
  return Promise.resolve();
};

const prepareRepo = async (repoFolder: string): Promise<any> => {
  // console.log(`preparing: ${repoFolder}`);
  chdir(repoFolder);
  let cmd = run(
    { ...standardCommandArgs, cmd: ["rm", "-f", ".git/index.lock"] },
  );
  const { code: removalCode } = await cmd.status();
  await errorHandler(removalCode, cmd);
  await cmd.close();
  cmd = run(
    { ...standardCommandArgs, cmd: ["git", "branch", "--show-current"] },
  );
  const { code: prevCode } = await cmd.status();
  const output = await cmd.output();
  const prevBranch = new TextDecoder().decode(output).trim();
  console.log(`prev branch: ${prevBranch} in ${repoFolder}`);
  await errorHandler(prevCode, cmd);
  await cmd.close();

  cmd = run({ ...standardCommandArgs, cmd: ["git", "status", "-s"] });
  const { code: statusCode } = await cmd.status();
  const statusOutput = await cmd.output();
  const changed = new TextDecoder().decode(statusOutput).trim();
  cmd.close();
  console.log(`changed: ${changed}`);
  let stashed = false;

  if (changed.length) {
    console.log(`stashing: ${repoFolder}`);
    cmd = run({ ...standardCommandArgs, cmd: ["git", "stash"] });
    const { code: stashCode } = await cmd.status();
    await errorHandler(stashCode, cmd);
    stashed = stashCode === 0;
    await cmd.close();
  }

  if (prevBranch !== "master") {
    console.log(`checkout master in: ${repoFolder}`);
    cmd = run({ ...standardCommandArgs, cmd: ["git", "checkout", "master"] });
    const { code: masterCode } = await cmd.status();
    await errorHandler(masterCode, cmd);
    await cmd.close();
  }

  cmd = run(
    { ...standardCommandArgs, cmd: ["git", "pull", "mender", "master"] },
  );
  const { code: pullCode } = await cmd.status();
  await errorHandler(pullCode, cmd);
  await cmd.close();
  return Promise.resolve({ prevBranch, stashed });
};

const processRepo = async (repo: string) => {
  console.log(`processing: ${repo}`);
  try {
    const gitInfo = await Deno.lstat(`${repo}/.git`);
    if (!gitInfo.isDirectory) {
      return Promise.resolve();
    }
  } catch (err) {
    console.log(`no git repo in: ${repo}`);
    return Promise.resolve();
  }
  const repoName = await getRepoName(repo);
  if (!repoName.length) {
    return Promise.resolve();
  }
  console.log(`repo name is: ${repoName}`);
  const { prevBranch, stashed } = await prepareRepo(repo);
  try {
    const fileInfo = await Deno.lstat(`${repo}/.dependabot`);
    if (!fileInfo.isDirectory) {
      console.log(`no dependabot in: ${repo}`);
      return Promise.resolve();
    }
  } catch (err) {
    console.log(`no dependabot in: ${repo}`);
    return Promise.resolve();
  }
  await createBranch(repo);
  await adjustDependabotFiles(repo);
  await createPr(repo, repoName);
  await restoreRepo(repo, prevBranch, stashed);
};

for (const thing of readDirSync(folder)) {
  const { name: dir } = thing;
  if (dir.startsWith("mender")) {
    await processRepo(`${folder}/${dir}`);
    thing.close();
  } else {
    thing.close();
  }
}
