#!/usr/bin/python3

import argparse
from collections import OrderedDict
import datetime
import os
import sys
import urllib.parse
import urllib.request
import lxml.etree

JENKINS_URL = "https://ci.cfengine.com"
WEEKLY_VIEW_NAME = "weekly"
NIGHTLY_VIEW_NAME = "Nightlies"

RESULT_ICONS = {
    "SUCCESS": "🟩",
    "FAILURE": "🟥",
    "UNSTABLE": "🟧",
    "ABORTED": "❌",
    None: "🫣",
}

HLINE = "----------------------------------------"

def _get_api_response(url, method="GET", payload=None):
    request = urllib.request.Request(url=url, data=payload, method=method)
    print("Requesting '%s'" % url, file=sys.stderr)
    with urllib.request.urlopen(request) as req:
        if not (200 <= req.status < 300):
            print("%s request to '%s' failed with status: %d [%s]" % (method, url,
                                                                      req.status, req.reason),
                  file=sys.stderr)
            return None
        else:
            return req.read()

def _get_last_job_result(view_name, job_name):
    params = urllib.parse.urlencode({"xpath": "/*/build[1]/url"})
    url = JENKINS_URL + "/view/" + view_name + "/job/" + job_name + ("/api/xml?%s" % params)
    build_url_data = _get_api_response(url)
    build_url = lxml.etree.fromstring(build_url_data.decode("utf-8"))

    url = build_url.text + "api/xml"
    build_data = _get_api_response(url)
    build = lxml.etree.fromstring(build_data.decode("utf-8"))

    build_info = OrderedDict()
    build_info["url"] = build_url.text
    result = build.find("result")
    build_info["result"] = result.text if result is not None else None
    build_info["jobs"] = OrderedDict()
    for sub_build in build.findall("subBuild"):
        job_info = OrderedDict()
        result = sub_build.find("result")
        job_info["result"] = result.text if result is not None else None
        job_info["url"] = JENKINS_URL + "/" + sub_build.find("url").text
        job_info["output"] = JENKINS_URL + "/" + sub_build.find("url").text + "console"
        build_info["jobs"][sub_build.find("jobName").text] = job_info

    return build_info

def _print_known_issues(summary_data):
    summary = lxml.etree.fromstring(summary_data.decode("utf-8"))
    summary_html_data = summary.find("text").text
    summary_html = lxml.etree.fromstring("<summary>" + summary_html_data + "</summary>")
    texts = [txt for txt in summary_html.itertext()]
    for known_issue in summary_html.findall("a"):
        issue_ticket = known_issue.text
        issue_info = texts[texts.index(issue_ticket) + 1].strip()
        print("*Known issue:* [%s](%s): %s" % (known_issue.text, known_issue.get("href"), issue_info))

def _print_subjob_info(job, subjob, failed):
    # something like https://ci.cfengine.com/view/Nightlies/job/3.18.x-nightly-pipeline/754/
    job_url = job["url"].strip("/")
    pipeline = job_url.split("/")[-2]
    if pipeline.endswith("pipeline"):
        pipeline = pipeline[:(-len("pipeline") - 1)]
    print("*Pipeline:* [%s](%s)" % (pipeline, job_url))
    subjob_url = subjob["url"].strip("/")
    job_id = "-".join(subjob_url.split("/")[-2:])
    print("*Job:* [%s](%s)" % (job_id, subjob_url))
    print("*Output:* %s" % subjob["output"])

    if failed:
        params = urllib.parse.urlencode({"xpath": "//status[(text() != 'PASSED') and (text() != 'SKIPPED') and (text() != 'FIXED')]/..",
                                         "wrapper": "failures"})
        url = subjob["url"] + "testReport/api/xml?%s" % params
        try:
            failures_data = _get_api_response(url)
        except urllib.error.HTTPError:
            print("*No additional info about the particular failures. Please fill it in manually*")
            return

        failures = lxml.etree.fromstring(failures_data.decode("utf-8"))
        for failure in failures.getchildren():
            name = failure.find("name").text
            print("*Test:* %s" % name)
            test_class = failure.find("className").text
            canonical_class = test_class.replace("/", "_")
            canonical_name = name.replace(".", "_").replace("-", "_")
            print("*Output:* %s" % (subjob["url"] + "testReport/(root)/" +
                                    canonical_class + "/" + canonical_name))
    else:
        # flakes
        subjob_id = subjob["url"].strip("/").split("/")[-1]
        params = urllib.parse.urlencode({"xpath": "/*/action[@_class = 'com.jenkinsci.plugins.badge.action.BadgeSummaryAction']"})
        url = subjob["url"] + "api/xml?%s" % params
        try:
            summary_data = _get_api_response(url)
        except urllib.error.HTTPError:
            # the flake can be in one of the runs on platforms if this a
            # multi-config job, not in this single/top-level job itself (see
            # below)
            pass
        else:
            _print_known_issues(summary_data)
            return

        params = urllib.parse.urlencode({"xpath": "/*/run/url", "wrapper": "urls"})
        url = subjob["url"] + "/api/xml?%s" % params
        try:
            runs_data = _get_api_response(url)
        except urllib.error.HTTPError:
            print("*Could not get more info about the flaked build, check it manually.*")
            return

        found_known_issue = False
        runs = lxml.etree.fromstring(runs_data.decode("utf-8"))
        for job_url in (url.text for url in runs.findall("url")):
            # job_url is something like https://ci.cfengine.com/job/testing-pr/label=PACKAGES_x86_64_linux_ubuntu_16/33106/

            # NOTE: jenkins can give us runs on platforms/configs from a different job if this
            #       particular job didn't run there, we need to filter those out
            params = urllib.parse.urlencode({"xpath": "//upstreamBuild"})
            url = job_url + "api/xml?%s" % params
            upstream_build_data = _get_api_response(url)
            upstream_build = lxml.etree.fromstring(upstream_build_data.decode("utf-8"))
            if upstream_build.text != subjob_id:
                continue

            params = urllib.parse.urlencode({"xpath": "/*/action[@_class = 'com.jenkinsci.plugins.badge.action.BadgeSummaryAction']"})
            url = job_url + "api/xml?%s" % params
            try:
                summary_data = _get_api_response(url)
            except urllib.error.HTTPError:
                # no result, not the flaked one
                continue
            else:
                found_known_issue = True
                platform = job_url.strip("/").split("/")[-2]
                if platform.startswith("label="):
                    platform = platform[len("label="):]
                print("*Platform:* [%s](%s)" % (platform, job_url))
                _print_known_issues(summary_data)

        if not found_known_issue:
            print("*Could not get more info about the flaked build, check it manually.*")


def print_jobs_status_info(weekly):
    view_name = WEEKLY_VIEW_NAME if weekly else NIGHTLY_VIEW_NAME
    params = urllib.parse.urlencode({"xpath": "//job/url[contains(text(), 'job')]",
                                     "wrapper": "job-urls"})
    url = JENKINS_URL + "/view/" + view_name + ("/api/xml?%s" % params)
    job_urls_data = _get_api_response(url)
    job_urls = lxml.etree.fromstring(job_urls_data.decode("utf-8"))
    jobs = dict()
    for job_url in job_urls.getchildren():
        # job_url.text is something like "https://ci.cfengine.com/job/3.18.x-nightly-pipeline/"
        job_name = job_url.text.strip("/").split("/")[-1]
        jobs[job_name] = _get_last_job_result(view_name, job_name)

    print("# QA status: ", end='')
    for job in jobs.values():
        print(RESULT_ICONS[job["result"]], end='')
        sys.stdout.flush()
    print()

    print("*Type:* %s" % ("Weekly" if weekly else "Nightly"))
    print("*Date:* %s" % datetime.datetime.now().date())
    print()

    failed = [job for job in jobs.values() if job["result"] == "FAILURE"]
    flaked = [job for job in jobs.values() if job["result"] == "UNSTABLE"]
    aborted = [job for job in jobs.values() if job["result"] == "ABORTED"]
    if any(failed + aborted):
        print("## Failed jobs")
        for job in (failed + aborted):
            for subjob in (subjob for subjob in job["jobs"].values()
                           if subjob["result"] == "FAILURE"):
                print(HLINE)
                _print_subjob_info(job, subjob, True)
        print(HLINE)
        print()

    if any(flaked + aborted):
        print("## Flaked jobs")
        for job in (flaked + aborted):
            for subjob in (subjob for subjob in job["jobs"].values()
                           if subjob["result"] == "UNSTABLE"):
                print(HLINE)
                _print_subjob_info(job, subjob, False)
        print(HLINE)
        print()

def main():
    ap = argparse.ArgumentParser(
        description="A helper tool for reporting status of jenkins jobs",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    group = ap.add_mutually_exclusive_group(required=False)
    group.add_argument("--nightly", help="Get results of nightly jobs", action="store_true", default=False)
    group.add_argument("--weekly", help="Get results of weekly jobs", action="store_true", default=False)
    group.add_argument("--debug", help="Print some debug information", action="store_true", default=False)

    args = ap.parse_args()

    if args.nightly and args.weekly:
        print("Cannot specify both --nightly and --weekly at the same time", file=sys.stderr)
        sys.exit(1)
    elif not args.nightly and not args.weekly:
        now = datetime.datetime.now()
        weekly = now.isoweekday() == 1 # Monday
    else:
        weekly = args.weekly

    if not args.debug:
        f = open("/dev/null", "w")
        sys.stderr = f

    success = print_jobs_status_info(weekly)
    sys.exit(0 if success else 1)

if __name__ == "__main__":
    main()
